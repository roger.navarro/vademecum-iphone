//
//  CompaniesVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 9/19/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import CoreData

class CompaniesVC: UIViewController {
  
  //MARK: - IBOutlets
  @IBOutlet weak var companiesTableView: UITableView!
  
  //MARK: - Properties
  var managedContext: NSManagedObjectContext!
  var fetchedResultsController: NSFetchedResultsController<NSDictionary>!
  
  //MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    loadCompanies()
    //remove more info button
    navigationItem.rightBarButtonItem = nil
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowCompany" {
      let viewController = segue.destination as! CompanyVC
      let indexPath = companiesTableView.indexPathForSelectedRow
      let company = fetchedResultsController.object(at: indexPath!)
      
      let name = company.value(forKey: "name") as! String
      viewController.mainTitle = name
      if let company = Company.getCompanyBy(name: name, in: managedContext) {
        viewController.company = company
        viewController.managedContext = managedContext
      } else {
        fatalError("🔴 Error while trying to company information")
      }
    }
  }
  
  //MARK: - IBActions
  @IBAction func showMoreInformation(_ sender: UIBarButtonItem) {
    let alert =
      UIAlertController(title: "Información de laboratorios" ,
                        message: "Los laboratorios listados en este menú han" +
                                 " otorgado el permiso a Vademécum Bolivia" +
                                 " para mostrar la información de sus" +
                                 " productos",
                        preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alert.addAction(okAction)
    present(alert, animated: true, completion: nil)
  }
  
  //MARK: - Other Functions
  func loadCompanies() {
    let companiesFetchRequest =
      NSFetchRequest<NSDictionary>(entityName: "Company")
    let nameAscSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
    let predicate =
      NSPredicate(format: "companyProducts.@count > 0 OR productsAsLaboratory.@count > 0")
    companiesFetchRequest.predicate = predicate
    companiesFetchRequest.sortDescriptors = [nameAscSortDescriptor]
    companiesFetchRequest.fetchBatchSize = 8
    let propertiesToFetch = ["name"]
    companiesFetchRequest.propertiesToFetch = propertiesToFetch
    fetchedResultsController =
      NSFetchedResultsController(fetchRequest: companiesFetchRequest,
                                 managedObjectContext: managedContext,
                                 sectionNameKeyPath: nil, cacheName: "all")
    
    do {
      try fetchedResultsController.performFetch()
    } catch let error as NSError {
      print("Could not fetch \(error), \(error.userInfo)")
    }
  }
}

//MARK: - UITableViewDataSource
extension CompaniesVC: UITableViewDataSource {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    let companiesInfo = fetchedResultsController.sections![section]
    return companiesInfo.numberOfObjects
  }
  
  func tableView(_ tableView: UITableView,
                 cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellIdentifier = "CompaniesCell"
    let cell =
      companiesTableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                             for: indexPath)
    let company = fetchedResultsController.object(at: indexPath) 
    let imageView: UIImageView = cell.viewWithTag(1000) as! UIImageView
    let imageData: NSData? = company.value(forKey: "image") as? NSData
    if let image = imageData {
      imageView.image = UIImage(data: image as Data)
    } else {
      imageView.image = nil
    }
    let label = cell.viewWithTag(100) as! UILabel
    label.text = company.value(forKey: "name") as? String
    
    return cell
  }
}

//MARK: - UITableViewDelegate
extension CompaniesVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 didSelectRowAt indexPath: IndexPath) {
    companiesTableView.deselectRow(at: indexPath, animated: true)
  }
}
