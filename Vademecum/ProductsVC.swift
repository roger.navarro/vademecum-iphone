//
//  ProductsVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/5/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import CoreData
import Firebase

let productViewControllerSegueIdentifier = "toProductViewController"

class ProductsVC: UIViewController {
  
  //MARK: - IBOutlets
  @IBOutlet weak var productsTableView: UITableView!

  //MARK: - Properties
  var managedContext: NSManagedObjectContext!
  var fetchedResultsController: NSFetchedResultsController<NSDictionary> =
    NSFetchedResultsController()
  let productsFetchRequest = NSFetchRequest<NSDictionary>(entityName: "Product")
  let nameAscSortDescriptor =
    NSSortDescriptor(key: "name",
                     ascending: true,
                     selector: #selector(NSString.localizedStandardCompare(_:)))
  var predicate: NSPredicate?
  var numberOfProductsFound: Int!
  var searchBarText: String!
  var isTheUserSearching = false
  
  //MARK: - Life cycle methods
  override func viewDidLoad() {
    super.viewDidLoad()
    
    productsTableView.estimatedRowHeight = 65.0;
    productsTableView.rowHeight = UITableViewAutomaticDimension;
    loadProducts()
    productsFetchRequest.fetchBatchSize = 15
    
    let cellNib = UINib(nibName: "EmptyCell", bundle: nil)
    productsTableView.register(cellNib, forCellReuseIdentifier: "EmptyCell")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    sayHiToUserIfNeeded()
  }
  
  @IBAction func unwindToVenuListViewController(_ segue: UIStoryboardSegue) {
  }
  
  deinit {
    print("*** deinit \(self)")
  }
  
  //MARK: - Other functions
  func loadProducts() {
    productsFetchRequest.sortDescriptors = [nameAscSortDescriptor]
    if predicate == nil {
      productsFetchRequest.predicate = NSPredicate(format: "isVisible == YES")
    } else {
      productsFetchRequest.predicate = predicate
    }
    
    productsFetchRequest.propertiesToFetch = ["id",
                                              "name",
                                              "solutionTypes",
                                              "therapeuticCategory",
                                              "lab"]
    
    fetchedResultsController =
      NSFetchedResultsController(fetchRequest: productsFetchRequest,
                                 managedObjectContext: managedContext,
                                 sectionNameKeyPath: nil, cacheName: "all")
//            let fetchRequest = NSFetchRequest<Product>(entityName: "Product")
//            fetchRequest.resultType = .countResultType
    
    
    do {
      try fetchedResultsController.performFetch()
//    let result = 
//      try managedContext.executeFetchRequest(fetchRequest) as! [NSNumber]
//    print(result.first!.integerValue)
    } catch let error as NSError {
      print("Could not fetch \(error), \(error.userInfo)")
    }
    
  }
  
  func sayHiToUserIfNeeded() {
    let defaults = UserDefaults.standard
    
    let isJustLoggedIn = defaults.bool(forKey: "justLoggedIn")
    if isJustLoggedIn {
      let hudView = HudView.hud(inView: self.view, animated: true)
      if let userName = FIRAuth.auth()?.currentUser?.displayName {
        hudView.text = "Saludos! \(userName)" 
      } else {
        hudView.text = "Bienvenido a Vademécum Bolivia"
      }
      defaults.set(false, forKey: "justLoggedIn")
    }
  }
  
  func addConstraints(fromView newView: UIView, toView view: UIView) {
    let left = NSLayoutConstraint(item: newView, attribute: .leading,
                                  relatedBy: .equal, toItem: view,
                                  attribute: .leading, multiplier: 1,
                                  constant: 0)
    let right = NSLayoutConstraint(item: newView,
                                   attribute: .trailing, relatedBy: .equal,
                                   toItem: view, attribute: .trailing ,
                                   multiplier: 1, constant: 0)
    let bottom = NSLayoutConstraint(item: newView, attribute: .bottom,
                                    relatedBy: .equal, toItem: view,
                                    attribute: .bottom , multiplier: 1,
                                    constant: 0)
    let top = NSLayoutConstraint(item: newView, attribute: .top,
                                 relatedBy: .equal, toItem: view,
                                 attribute: .top, multiplier: 1,
                                 constant: 0)
    top.priority = 999
    bottom.priority = 999
    view.addConstraints([left,right,bottom,top])
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == productViewControllerSegueIdentifier {
      let productViewController = segue.destination as! ProductVC
      let indexPath = productsTableView.indexPathForSelectedRow!
      let product = fetchedResultsController.object(at: indexPath)
      
      let childContext =
        NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
      childContext.parent = managedContext
      
      productViewController.managedContext = childContext
      productViewController.productID = product.value(forKey: "id") as! String
      productViewController.mainTitleStr = product.value(forKey: "name") as? String
      
      storeProductIDInNSUserDefaults(product.value(forKey: "id") as! String)
    }
  }
  
  func fetchAndReload(_ fetchRequest: NSFetchRequest<NSDictionary>) {
    fetchedResultsController =
      NSFetchedResultsController(fetchRequest: fetchRequest,
                                 managedObjectContext: managedContext,
                                 sectionNameKeyPath: nil, cacheName: "search")
    do {
      try fetchedResultsController.performFetch()
      productsTableView.reloadData()
    } catch let error as NSError {
      print("Could not fetch \(error), \(error.userInfo)")
    }
  }
}

//MARK: - UITableViewDataSource
extension ProductsVC: UITableViewDataSource {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    let sectionInfo = fetchedResultsController.sections![section]
    numberOfProductsFound = sectionInfo.numberOfObjects
    if numberOfProductsFound == 0 {
      return 1
    } else {
      return numberOfProductsFound
    }
  }
  
  func tableView(_ tableView: UITableView,
                 cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if numberOfProductsFound == 0 {
      let cell =
        tableView.dequeueReusableCell(withIdentifier: "EmptyCell",
                                      for: indexPath) as! NothingFoundCell
      var text = ""
      if let sbText = searchBarText {
        text = "No se encontró ningún producto llamado \"\(sbText)\""
      } else {
        text = "No se encontró ningún producto"
      }
      cell.configureCell(description: text)
      return cell
    }
    
    let cellIdentifier = "ProductCell"
    let cell =
      productsTableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                            for: indexPath)
    let product = fetchedResultsController.object(at: indexPath) 
    let titleLabel = cell.viewWithTag(1000) as! UILabel
    
    if isTheUserSearching {
      var value = product.value(forKey: "name") as! String
      if value.contains("´") {
        value = value.replacingOccurrences(of: "´", with: "'")
      }
      titleLabel.attributedText = highlightMatches(searchBarText,
                                                   inString: value)
    } else {
      titleLabel.attributedText =
        NSAttributedString(string: product.value(forKey: "name") as! String)
    }
    
    let solutionTypesLabel = cell.viewWithTag(1001) as! UILabel
    let solutionTypesValue = product.value(forKey: "solutionTypes") as! String
    solutionTypesLabel.attributedText =
      NSAttributedString(string: solutionTypesValue)
    
    let therapeuticCategoryLabel = cell.viewWithTag(1002) as! UILabel
    let therapeuticCategoryValue =
      product.value(forKey: "therapeuticCategory") as! String
    therapeuticCategoryLabel.attributedText =
      NSAttributedString(string: therapeuticCategoryValue)
    
    let labLabel = cell.viewWithTag(1003) as! UILabel
    labLabel.attributedText =
      NSAttributedString(string: product.value(forKey: "lab") as! String)
    
    return cell
  }
  
  func tableView(_ tableView: UITableView,
                 willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    return indexPath
  }
  
}

//MARK: UITableViewDelegate
extension ProductsVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 didSelectRowAt indexPath: IndexPath) {
    
    productsTableView.deselectRow(at: indexPath, animated: true)
  }
}

//MARK: - UISearchBarDelegate
extension ProductsVC: UISearchBarDelegate {
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    self.searchText(searchText)
  }
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    searchBar.setShowsCancelButton(true, animated: true)
    searchBar.placeholder = "Ej. Digestan"
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    UIView.animate(withDuration: 0.5, animations: {
      searchBar.setShowsCancelButton(false, animated: true)
      searchBar.resignFirstResponder()
    }, completion: { _ in
      if searchBar.text! != "" {
        searchBar.text = ""
        self.searchText("")
      }
    })
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    searchBar.placeholder = "Buscar producto..."
  }
  
  func searchText(_ text: String) {
    searchBarText = text
    if text != "" {
      productsFetchRequest.predicate =
        NSPredicate(format: "name contains[cd] %@ AND isVisible == YES", text)
      isTheUserSearching = true
    } else {
      productsFetchRequest.predicate =
        NSPredicate(format: "isVisible == YES")
      isTheUserSearching = false
    }
    fetchAndReload(productsFetchRequest)
  }
}
