//
//  CompanyVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 9/21/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import CoreData

class CompanyVC: UIViewController {
  
  @IBOutlet weak var productsContainer: UIView!
  @IBOutlet weak var informationContainer: UIView!
  @IBOutlet weak var content: UIView!
  @IBOutlet weak var segmentControl: UISegmentedControl!
  
  var company: Company!
  var managedContext: NSManagedObjectContext!
  
  enum TableType{
    case information
    case products
  }
  
  var mainTitle: String? {
    didSet {
      self.title = mainTitle
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    content.addSubview(informationContainer)
    UIViewHelper().addConstraints(fromView: informationContainer,
                                  toView: content)
  }
  
  @IBAction func segmentChanged(_ sender: UISegmentedControl) {
    switch sender.selectedSegmentIndex {
    case 0:
      removeTable(.products)
      content.addSubview(informationContainer)
      UIViewHelper().addConstraints(fromView: informationContainer,
                                    toView: content)
      
      
    case 1:
      removeTable(.information)
      content.addSubview(productsContainer)
      UIViewHelper().addConstraints(fromView: productsContainer,
                                    toView: content)
    default:
      break
    }
  }
  
  func removeTable(_ tableType: TableType) {
    switch tableType {
    case .information:
      for view in content.subviews {
        if view.tag == 100 {//informationTable
          view.removeFromSuperview()
          break
        }
      }
    case .products:
      for view in content.subviews {
        if view.tag == 1000 { //productsTable
          view.removeFromSuperview()
          break
        }
      }
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    switch segue.identifier! {
    case "ShowCompanyInformation":
      let companyInformationTableViewController =
        segue.destination as! CompanyInformationTVC
      companyInformationTableViewController.company = company
    case "ShowCompanyProducts":
      let productsViewController = segue.destination as! ProductsVC
      productsViewController.predicate =
        NSPredicate(format: "isVisible == YES AND (company.name == %@ OR laboratory.name ==%@)",
                    company.name, company.name)
      productsViewController.managedContext = managedContext
    default:
      break
    }
  }
}
