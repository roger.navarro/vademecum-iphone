//
//  ActivePrincipleVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/7/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import CoreData

class ActivePrincipleVC: UIViewController {
  
  //MARK: - IBOutlets
  @IBOutlet weak var activePrincipleTableview: UITableView!
  
  //MARK: - Properties
  var managedContext: NSManagedObjectContext!
  var fetchedResultsController: NSFetchedResultsController<NSDictionary>!
  let productsByActPrinFetchRequest =
    NSFetchRequest<NSDictionary>(entityName: "ActivePrinciple")
  let nameAscSortDescriptor =
    NSSortDescriptor(key: "name", ascending: true,
                     selector: #selector(NSString.localizedStandardCompare(_:)))
  var activePrinciplesFound: Int!
  var searchBarText: String!
  var isTheUserSearching = false
  
  //MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    activePrincipleTableview.estimatedRowHeight = 65.0
    activePrincipleTableview.rowHeight = UITableViewAutomaticDimension
    
    productsByActPrinFetchRequest.sortDescriptors = [nameAscSortDescriptor]
    productsByActPrinFetchRequest.predicate =
      NSPredicate(format: "ANY productList.isVisible == YES")
    productsByActPrinFetchRequest.fetchBatchSize = 15
    let propertiesToFetch = ["name"]
    productsByActPrinFetchRequest.propertiesToFetch = propertiesToFetch
    self.title = "Principio Activo"
    fetchedResultsController =
      NSFetchedResultsController(fetchRequest: productsByActPrinFetchRequest,
                                 managedObjectContext: managedContext,
                                 sectionNameKeyPath: nil, cacheName: "all")
    let cellNib = UINib(nibName: "EmptyCell", bundle: nil)
    activePrincipleTableview.register(cellNib,
                                      forCellReuseIdentifier: "EmptyCell")
    
    do {
      try fetchedResultsController.performFetch()
    } catch let error as NSError {
      print("Could not fetch \(error), \(error.userInfo)")
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "toProductsViewController" {
      let productsViewController = segue.destination as! ProductsVC
      productsViewController.managedContext = managedContext
      let indexPath = activePrincipleTableview.indexPathForSelectedRow!
      let activePrinciple = fetchedResultsController.object(at: indexPath) 
      productsViewController.predicate =
        NSPredicate(format: "%@ in activePrincipleList AND isVisible == YES",
                    activePrinciple)
      let name = activePrinciple.value(forKey: "name") as! String
      productsViewController.title = name
    }
  }
  
  func fetchAndReload(_ fetchRequest: NSFetchRequest<NSDictionary>) {
    fetchedResultsController =
      NSFetchedResultsController(fetchRequest: fetchRequest,
                                 managedObjectContext: managedContext,
                                 sectionNameKeyPath: nil, cacheName: "search")
    do {
      try fetchedResultsController.performFetch()
      activePrincipleTableview.reloadData()
    } catch let error as NSError {
      print("Could not fetch \(error), \(error.userInfo)")
    }
  }
  
  @IBAction func unwindToActivePrincipleListViewController(
    _ segue: UIStoryboardSegue) {
  }
}

//MARK: - UITableViewDataSource
extension ActivePrincipleVC: UITableViewDataSource {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    let sectionInfo = fetchedResultsController.sections![section]
    activePrinciplesFound = sectionInfo.numberOfObjects
    if activePrinciplesFound == 0 {
      return 1
    } else {
      return activePrinciplesFound
    }
  }
  
  func tableView(_ tableView: UITableView,
                 cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if activePrinciplesFound == 0 {
      let cell =
        tableView.dequeueReusableCell(withIdentifier: "EmptyCell",
                                      for: indexPath) as! NothingFoundCell
      cell.configureCell(
        description: "No se encontró ningún principio" +
                     " activo llamado \"\(searchBarText!)\"")
      return cell
    }
    
    let cellIdentifier = "ActivePrincipleCell"
    let cell =
      activePrincipleTableview.dequeueReusableCell(
        withIdentifier: cellIdentifier, for: indexPath)
    let activePrinciple = fetchedResultsController.object(at: indexPath)
    let name = activePrinciple.value(forKey: "name") as! String
    if isTheUserSearching {
      cell.textLabel!.attributedText = highlightMatches(searchBarText,
                                                        inString: name)
    } else {
      cell.textLabel!.attributedText = NSAttributedString(string: name)
    }
    
    return cell
  }
  
  func tableView(_ tableView: UITableView,
                 willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    return indexPath
  }
}

//MARK: - UITableViewDelegate
extension ActivePrincipleVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 didSelectRowAt indexPath: IndexPath) {
    activePrincipleTableview.deselectRow(at: indexPath, animated: true)
  }
}

//MARK: - UISearchBarDelegate
extension ActivePrincipleVC: UISearchBarDelegate {
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    self.searchText(searchText)
  }
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    searchBar.setShowsCancelButton(true, animated: true)
    searchBar.placeholder = "Ej. Paracetamol o Ibuprofeno"
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    searchBar.placeholder = "Buscar principio activo..."
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.setShowsCancelButton(false, animated: true)
    searchBar.resignFirstResponder()
    if searchBar.text! != "" {
      searchBar.text = ""
      self.searchText("")
    }
  }
  
  func searchText(_ text: String) {
    searchBarText = text
    let searchProductsFetchRequest =
      NSFetchRequest<NSDictionary>(entityName: "ActivePrinciple")
    if text != "" {
      searchProductsFetchRequest.predicate =
        NSPredicate(format: "name contains[cd] %@ AND ANY" +
          " productList.isVisible == YES", text)
      isTheUserSearching = true
    } else {
      searchProductsFetchRequest.predicate =
        NSPredicate(format: "ANY productList.isVisible == YES", text)
      isTheUserSearching = false
    }
    searchProductsFetchRequest.sortDescriptors = [nameAscSortDescriptor]
    fetchAndReload(searchProductsFetchRequest)
  }
}
