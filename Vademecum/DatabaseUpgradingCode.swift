//
//  DatabaseUpgradingCode.swift
//  Vademecum
//
//  Created by Roger Navarro on 7/29/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import CoreData

func improveHealthRecord(_ coreDataStack: CoreDataStack) {
  
  let productsFR = NSFetchRequest<NSFetchRequestResult>(entityName: "Product")
  //productsFR.predicate = NSPredicate(format: "healthRecord != \"\"")
  let productsFRResult = try! coreDataStack.mainContext.fetch(productsFR) as! [Product]
  for product in productsFRResult {
    if product.healthRecord == nil {
      continue
    } else {
      var healthRecordString = product.healthRecord!
      var healthRecords:[String] = listMatches("[0-9]/2[0-9][0-9][0-9]", inString: healthRecordString)
      healthRecords = healthRecords + listMatches("[0-9] /2[0-9][0-9][0-9]", inString: healthRecordString)
      healthRecords = healthRecords + listMatches("[0-9] / 2[0-9][0-9][0-9]", inString: healthRecordString)
      for healthRecord in healthRecords {
        let newHealthRecord = healthRecord.replacingOccurrences(of: "/", with: " Año: ")
        let newHelthRecordString = healthRecordString.replacingOccurrences(of: healthRecord, with: newHealthRecord)
        healthRecordString = newHelthRecordString
      }
      product.healthRecord = healthRecordString
      
      do {
        try coreDataStack.mainContext.save()
      } catch let error as NSError {
        print("insert error: \(error.localizedDescription)")
      }
    }
  }
}

var dosageFormDictionary = ["Ampolla":"ampolla","CAPSULA":"cápsula","Comprimido":"comprimido","COMPRIMIDO":"comprimido","Cápsula":"cápsula","CÁPSULA":"cápsula","Gragea":"gragea","Granulado":"granulado","Infusor":"infusor","Jeringa":"jeringa","Parche":"parche","Sobre":"sobre","Supositorio":"supositorio","Tableta":"tableta","Tabletas":"tableta","Óvulo":"óvulo"]

func replaceStringWithDictionary(_ dictionary: [String:String], withString string: String) -> String {
  var stringToReturn = string
  for key in dictionary.keys {
    if string.contains(key) {
      stringToReturn = string.replacingOccurrences(of: key, with: dictionary[key]!)
      print("Dictionary cases: " + stringToReturn)
      break
    } else {
      continue
    }
  }
  return stringToReturn
}

func improveDosageForm(_ coreDataStack: CoreDataStack) {
  let productsFR = NSFetchRequest<NSFetchRequestResult>(entityName: "Product")
  let productsFRResult = try! coreDataStack.mainContext.fetch(productsFR) as! [Product]
  for product in productsFRResult {
    if product.dosageForm == nil {
      continue
    } else {
      var dosageFormString = product.dosageForm!
      var phrasesThatMatter:[String] = listMatches("Cada *?.*? contiene:", inString: dosageFormString)
      phrasesThatMatter = phrasesThatMatter + listMatches("Cada *?.*? contiene :", inString: dosageFormString)
      phrasesThatMatter = phrasesThatMatter + listMatches("Cada *?.*? contienen:", inString: dosageFormString)
      phrasesThatMatter = uniqElementsInArray(phrasesThatMatter)
      for phrase in phrasesThatMatter where phrasesThatMatter != []  {
        var newPhrase = phrase.condenseWhitespace()
        newPhrase = replaceStringWithDictionary(dosageFormDictionary, withString: newPhrase)
        newPhrase = newPhrase.replacingOccurrences(of: "Cada", with: "En cada")
        newPhrase = newPhrase.replacingOccurrences(of: "contiene:", with: "hay:")
        newPhrase = newPhrase.replacingOccurrences(of: "contienen:", with: "hay:")
        let newDosageFormString = dosageFormString.replacingOccurrences(of: phrase, with: newPhrase)
        dosageFormString = newDosageFormString
      }
      print("New String: " + dosageFormString)
      product.dosageForm = dosageFormString
      
      do {
        try coreDataStack.mainContext.save()
      } catch let error as NSError {
        print("insert error: \(error.localizedDescription)")
      }
    }
  }
}

func joinProductsWithCompanies(managedContext: NSManagedObjectContext) {
  let companiesFetchRequest =
    NSFetchRequest<Company>(entityName: "Company")
  do {
    let companies = try managedContext.fetch(companiesFetchRequest)
    let productsFetchRequest = NSFetchRequest<Product>(entityName: "Product")
    
    for company in companies {
      let predicate = NSPredicate(format: "lab == %@ OR distributor == %@", company.name, company.name)
      productsFetchRequest.predicate = predicate
      do {
        let products = try managedContext.fetch(productsFetchRequest)
        for product in products {
          if let lab = product.lab, lab == company.name {
            product.laboratory = company
          } else if let distributor = product.distributor, distributor == company.name {
            product.company = company
          }
        }
        do {
          try managedContext.save()
        } catch {
          print("\(error), \(error.localizedDescription)")
        }
      } catch {
        print("Error while retriving products: " +
          "\(error), \(error.localizedDescription)")
      }
    }
  } catch {
    print("Error while retriving Data: \(error), \(error.localizedDescription)")
  }
}
