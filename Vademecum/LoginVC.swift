//
//  LoginVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 1/16/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import CoreData
import FBSDKCoreKit
import FBSDKLoginKit

class fbCustomLoginButton: FBSDKLoginButton {
    
  override func setTitle(_ title: String?, for state: UIControlState) {
    super.setTitle("Iniciar sesión con Facebook", for: .normal)
    }
}

class LoginVC: UIViewController, IAPContainer {
  
  //MARK: - IBOutlets
  @IBOutlet weak var fbLoginButton: fbCustomLoginButton!
  @IBOutlet weak var googleLoginButton: GIDSignInButton!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var loginStatusLabel: UILabel!
  @IBOutlet weak var loginButton: UIButton!
  @IBOutlet weak var createUserButton: UIButton!
  @IBOutlet weak var mainView: UIView!
  @IBOutlet weak var googleCoverView: UIView!
  
  //MARK: - Properties
  var managedContext: NSManagedObjectContext!
  var emitter = CAEmitterLayer()
  var iapHelper: IAPHelper? {
    didSet {
      updateIAPHelper()
    }
  }
  fileprivate var adRemovalProduct: SKProduct?
  
  //MARK: - View Life Cycle
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    activityIndicator.isHidden = true
    loginStatusLabel.isHidden = true
    
    GIDSignIn.sharedInstance().delegate = self
    GIDSignIn.sharedInstance().uiDelegate = self
    GIDSignIn.sharedInstance().language = "es"
    
    fbLoginButton.delegate = self
    fbLoginButton.readPermissions = ["public_profile", "email"]
    
    NotificationCenter.default.addObserver(self,
                                           selector:
      #selector(LoginVC.handlePurchaseNotification(_:)),
                                           name:
      NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
                                           object: nil)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    animateImagesInTheBrackgroud()
    
    if let user = FIRAuth.auth()?.currentUser{
      viewWillLogIn()
      switch user.providerData[0].providerID {
//      case "google.com":
//        GIDSignIn.sharedInstance().signInSilently()
      case "password":
        if user.isEmailVerified {
          performSegue(withIdentifier: "ShowTabBar", sender: nil)
          if let email = user.email {
            let defaults = UserDefaults.standard
            defaults.set(email, forKey: "justLoggedInEmail")
          } else {
            print("Error: No email was set in firebase Login")
          }
        } else {
          viewDidLogedIn()
        }
      default:
        performSegue(withIdentifier: "ShowTabBar", sender: nil)
      }
    }
  }
  
  override func viewWillTransition(
    to size: CGSize,
    with coordinator: UIViewControllerTransitionCoordinator)
  {
    super.viewWillTransition(to: size, with: coordinator)
    coordinator.animate(alongsideTransition: {_ in
      self.emitter.removeFromSuperlayer()
    }, completion: { _ in
      self.animateImagesInTheBrackgroud()
    })
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowTabBar" {
      let tabController = segue.destination as! UITabBarController
      initTabBarView(tabController: tabController)
      emitter.removeFromSuperlayer()
      let defaults = UserDefaults.standard
      defaults.set(true, forKey: "justLoggedIn")
    }
  }
  
  // MARK: - IBActions
  
  @IBAction func skipLogIn(_ sender: Any) {
    performSegue(withIdentifier: "ShowTabBar", sender: nil)
  }
  
  @IBAction func unwindToLoginViewController(_ segue: UIStoryboardSegue) {
    if let user = FIRAuth.auth()?.currentUser{
      switch user.providerData[0].providerID {
      case "google.com":
        GIDSignIn.sharedInstance().disconnect()
        logOutFromFirebase()
        viewDidLogedIn()
      case "facebook.com":
        logOutFromFacebook()
        logOutFromFirebase()
        viewDidLogedIn()
      case "password":
        logOutFromFirebase()
        viewDidLogedIn()
      default:
        break
      }
    }
  }

  //MARK: - Other functions
  func initTabBarView(tabController: UITabBarController) {
    let tabBarRootViewControllers: Array = tabController.viewControllers!
    let navView0 = tabBarRootViewControllers[0] as! UINavigationController
    let navView3 = tabBarRootViewControllers[1] as! UINavigationController
    let navView4 = tabBarRootViewControllers[2] as! UINavigationController
    let searchVC = navView0.topViewController! as! SearchVC
    let companiesViewController =
      navView3.topViewController! as! CompaniesVC
    let moreInformationViewController =
      navView4.topViewController! as! MoreInformationVC
    searchVC.managedContext = managedContext
    moreInformationViewController.managedContext = managedContext
    companiesViewController.managedContext = managedContext
    UITabBarItem.appearance().setTitleTextAttributes(
      [NSForegroundColorAttributeName: UIColor(red: 119/255,
                                               green: 170/255,
                                               blue: 17/255,
                                               alpha: 1)], for: .selected)
    UITabBar.appearance().tintColor = UIColor(red: 119/255,
                                              green: 170/255,
                                              blue: 17/255,
                                              alpha: 1)
  }
  
  func logOutFromFacebook() {
    let fbManager = FBSDKLoginManager.init()
    fbManager.logOut()
    FBSDKAccessToken.setCurrent(nil)
  }
  
  func logOutFromFirebase() {
    let firebaseAuth = FIRAuth.auth()
    do {
      try firebaseAuth?.signOut()
    } catch {
      print("\n\nR-Error signing out: ", error)
    }
  }
  
  func viewWillLogIn() {
    activityIndicator.isHidden = false
    activityIndicator.startAnimating()
    fbLoginButton.isEnabled = false
    googleLoginButton.isEnabled = false
    googleCoverView.isHidden = true
    loginStatusLabel.isHidden = false
    loginButton.isEnabled = false
    createUserButton.isEnabled = false
    
  }
  
  func viewDidLogedIn() {
    activityIndicator.stopAnimating()
    activityIndicator.isHidden = true
    fbLoginButton.isEnabled = true
    googleLoginButton.isEnabled = true
    googleCoverView.isHidden = false
    loginStatusLabel.isHidden = true
    loginButton.isEnabled = true
    createUserButton.isEnabled = true
  }
  
  func showAlertMessage(msg: String) {
    let alert = UIAlertController(title: "Error de autentificación",
                                  message: msg,
                                  preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default,
                                 handler: nil)
    present(alert, animated: true, completion: nil)
    alert.addAction(okAction)
  }
  
  fileprivate func setAdsAsRemovedAndNoInternetConectionRequired(_ removed:
    Bool,
                                                                 animated:
    Bool = true)
  {
    DispatchQueue.main.async {
      if animated {
        UIView.animate(withDuration: 0.7, animations: {
          print("remove add")
        })
      } else {
        print("remove add 2")
      }
    }
  }
  
  private func updateIAPHelper() {
    passIAPHelperToChildren()
    
    guard let iapHelper = iapHelper else { return }
    
    iapHelper.requestProducts {
      products in
      self.adRemovalProduct = products!.filter {
        $0.productIdentifier ==
          ReceiptAppPurchase.NoInternetRequiredWithAdRemoval.productId
        }.first
    }
    
    setAdsAsRemovedAndNoInternetConectionRequired(
      iapHelper.purchasedProductIdentifiers.contains(
        ReceiptAppPurchase.NoInternetRequiredWithAdRemoval.productId),
      animated: false)
  }
  
  func handlePurchaseNotification(_ notification: Notification) {
    if let productID = notification.object as? String, productID ==
      ReceiptAppPurchase.NoInternetRequiredWithAdRemoval.productId {
      setAdsAsRemovedAndNoInternetConectionRequired(true)
    }
  }
}

// MARK: - Animations
extension LoginVC {
  fileprivate func animateImagesInTheBrackgroud() {
    let rect = CGRect(x: 0.0, y: -150.0, width: view.bounds.width, height: 150)
    emitter.frame = rect
    mainView.layer.sublayers?.insert(emitter, at: 0)
    emitter.emitterShape = kCAEmitterLayerRectangle
    emitter.emitterPosition = CGPoint(x: rect.width/2, y: rect.height/2)
    emitter.emitterSize = rect.size
    emitter.renderMode = kCAEmitterLayerBackToFront
    
    let labEmitterCell = CAEmitterCell()
    labEmitterCell.contents = UIImage(named: "lab-login")!.cgImage
    labEmitterCell.birthRate = 2
    labEmitterCell.lifetime = 15
    labEmitterCell.scale = 0.1
    labEmitterCell.yAcceleration = 20.0
    
    let activePrincipleEmitterCell = CAEmitterCell()
    activePrincipleEmitterCell.contents = UIImage(
      named: "activePrinciple-login")!.cgImage
    activePrincipleEmitterCell.birthRate = 2
    activePrincipleEmitterCell.lifetime = 15
    activePrincipleEmitterCell.scale = 0.1
    activePrincipleEmitterCell.yAcceleration = 20.0
    
    let historyEmitterCell = CAEmitterCell()
    historyEmitterCell.contents = UIImage(named: "history-login")!.cgImage
    historyEmitterCell.birthRate = 2
    historyEmitterCell.lifetime = 15
    historyEmitterCell.scale = 0.1
    historyEmitterCell.yAcceleration = 20.0
    
    let productEmitterCell = CAEmitterCell()
    productEmitterCell.contents = UIImage(named: "products-logIn")!.cgImage
    productEmitterCell.birthRate = 2
    productEmitterCell.lifetime = 15
    productEmitterCell.scale = 0.1
    productEmitterCell.yAcceleration = 20.0
    
    emitter.emitterCells = [labEmitterCell,
                            activePrincipleEmitterCell,
                            historyEmitterCell,
                            productEmitterCell]
  }
}

// MARK: - GIDSignInDelegate
extension LoginVC: GIDSignInDelegate, GIDSignInUIDelegate {
  
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
            withError error: Error?) {
    if let error = error {
      showAlertMessage(msg: error.localizedDescription)
      return
    }
    
    viewWillLogIn()
    
    guard let authentication = user.authentication else { return }
    let credential = FIRGoogleAuthProvider.credential(
      withIDToken: authentication.idToken,
      accessToken: authentication.accessToken)
    
    FIRAuth.auth()?.signIn(with: credential) { (fuser, error) in
      self.viewDidLogedIn()
      if let error = error {
        self.showAlertMessage(msg: error.localizedDescription)
        return
      } else {
        let defaults = UserDefaults.standard
        defaults.set(user.profile.email, forKey: "justLoggedInEmail")
        self.performSegue(withIdentifier: "ShowTabBar", sender: nil)
      }
    }
    
  }
  
  func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
            withError error: Error!) {
    if let error = error {
      self.showAlertMessage(msg: error.localizedDescription)
    } else {
      GIDSignIn.sharedInstance().signOut()
    }
  }
}

// MARK: - FBSDKLoginButtonDelegate
extension LoginVC: FBSDKLoginButtonDelegate {
  public func loginButton(_ loginButton: FBSDKLoginButton!,
                          didCompleteWith result: FBSDKLoginManagerLoginResult!,
                          error: Error!) {
    if let error = error {
      showAlertMessage(msg: error.localizedDescription)
      return
    } else if result.isCancelled {
      return
    } else {
      viewWillLogIn()
      let credential = FIRFacebookAuthProvider.credential(
        withAccessToken: FBSDKAccessToken.current().tokenString)
      FIRAuth.auth()?.signIn(with: credential) { (fuser, error) in
        self.viewDidLogedIn()
        let request = FBSDKGraphRequest.init(
          graphPath: "me",
          parameters: ["fields":"email,name"]
        )
        request?.start(completionHandler: {
          (graphRequestConnection, result, error) in
          if error != nil {
            print("Error: \(error), \(error?.localizedDescription)")
          } else {
            let defaults = UserDefaults.standard
            let optMail = (result as! Dictionary<String, String>)["email"]
            if let email = optMail {
              defaults.set(email, forKey: "justLoggedInEmail")
            } else {
              defaults.set(nil, forKey: "justLoggedInEmail")
            }
          }
        })
        if let error = error {
          self.showAlertMessage(msg: error.localizedDescription)
          return
        } else {
          self.performSegue(withIdentifier: "ShowTabBar", sender: nil)
        }
      }
    }
  }
  
  public func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
  }
}
