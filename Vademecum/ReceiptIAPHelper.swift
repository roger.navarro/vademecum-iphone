//
//  File.swift
//  ReceiptApp
//
//  Created by Roger Navarro on 2/16/17.
//  Copyright © 2017 Roger Navarro. All rights reserved.
//

import Foundation


enum ReceiptAppPurchase: String {
  case NoInternetRequiredWithAdRemoval = "NoInternetRequiredWithAdRemovall"
  
  var productId: String {
    return (Bundle.main.bundleIdentifier ?? "") + "." + rawValue
  }
  
  init?(productId: String) {
    guard let bundleID = Bundle.main.bundleIdentifier, productId.hasPrefix(bundleID) else {
      return nil
    }
    self.init(rawValue: productId.replacingOccurrences(of: bundleID + ".", with: ""))
  }
}
