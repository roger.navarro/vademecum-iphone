//
//  NothingFoundCell.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/13/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import UIKit

class NothingFoundCell: UITableViewCell {
  
  @IBOutlet weak var mainLabel: UILabel!
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
  }
  
  func configureCell(description: String) {
    mainLabel.text = description
  }
  
}
