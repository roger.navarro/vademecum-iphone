//
//  AboutVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 8/8/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {
  
  //MARK: - IBOutlets
  @IBOutlet weak var versionLabel: UILabel!
  
  //MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    setVersionLabel()
  }
  
  //MARK: - Other functions
  func setVersionLabel() {
    versionLabel.text = "Versión: \(UIApplication.appBuild())"
  }
}
