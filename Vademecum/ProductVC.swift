//
//  ProductVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/13/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class ProductVC: UIViewController {
  
  //MARK: - IBOutlets
  @IBOutlet weak var productTableView: UITableView!
  @IBOutlet weak var moreInformationButton: UIButton!
  @IBOutlet weak var moreInformationCell: UITableViewCell!
  @IBOutlet weak var bannerView: GADBannerView!
  
  //MARK: - Properties
  var managedContext: NSManagedObjectContext!
  var product: Product!
  var productID: String!
  var selectedIndexPath: IndexPath?
  var reloadFirstTable = true
  var sectionsWithRowHeight: [Int : CGFloat]?
  var mainTitleStr: String? {
    didSet {
      if let title = mainTitleStr {
        self.title = title
      }
    }
  }
  var otherSectionList: [Int] = []
  var titleList: [(String,String)] = []
  var detailList: [String] = []
  var dictTitleList: [Int: (String, String)] = [:]
  var dictDetailList: [Int: String] = [:]
  var titlesDictionary: [String:(index: Int, value: String)] = [
    "name":(0,"Producto"),
    "solutionTypes":(1,"Forma medicamentosa"),
    "therapeuticCategory":(2,"Categoría terapéutica"),
    "lab":(3,"Línea"),
    "dosageForm":(4,"Composición"),
    "distributor":(5,"Representante"),
    "productDescription":(6,"Descripción"),
    "therapeuticIndications":(7,"Indicaciones"),
    "humanPharmacokinetics":(8,"Acción farmacodinámica y farmacocinética"),
    "adverseReactions":(9,"Reaccion adversa y efecto secundario"),
    "generalPrecautions":(10,"Precauciones"),
    "useRestrictions":(11,"Contraindicaciones"),
    "contraindications":(11,"Contraindicaciones"),
    "drugInteractions":(12,"Interacción farmacológica"),
    "labAlterations":(13,"Resultados de pruebas de laboratorio"),
    "genesis":(14,"Recomendaciones sobre efectos carcinogenesis" +
      ", mutagenesis, teratogenesis y sobre la fertilidad"),
    "dosageAdministration":(15,"Posología y administración"),
    "overdoseManagement":(16,"Sobredosificación"),
    "protectionLegends":(17,"Almacenaje"),
    "storageRecomendations":(17,"Almacenaje"),
    "presentations":(18,"Presentaciones"),
    "healthRecord":(19,"Registro sanitario"),
    "isGeneric":(20,"Comercial / Genérico")
  ]
  
  //MARK: - View life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    if product != nil {
      loadData(product)
    } else {
      if let product = Product.getProductBy(id: productID, in: managedContext) {
        self.product = product
        loadData(product)
      }
    }
    loadBannerView()
    productTableView.rowHeight = UITableViewAutomaticDimension
    productTableView.estimatedRowHeight = 10.0
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if let heights = sectionsWithRowHeight?.sorted(by: {$0.0 < $1.0}) {
      if reloadFirstTable {
        if let section = heights.first?.0 {
          self.productTableView.reloadRows(
            at: [IndexPath(row: 0, section: section)], with: .automatic)
        }
        reloadFirstTable = false
      }
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowSection" {
      let sectionViewController =
        segue.destination as! DescriptionVC
      let indexPath = productTableView.indexPathForSelectedRow
      let index = indexPath!.row + detailList.count
      let title = titleList[index].1
      sectionViewController.productName = product.name
      sectionViewController.productDescriptionTitle = title
      var productDescription = ""
      if titleList[index].0 == "protectionLegends" ||
        titleList[index].0 == "storageRecomendations"
      {
        let storageRecomendation =
          product.value(forKey: "storageRecomendations") as! String
        let protectionLegends =
          product.value(forKey: "protectionLegends") as! String
        productDescription = (storageRecomendation == "") ?
          protectionLegends : storageRecomendation + "<br><br>" +
        protectionLegends
      } else if titleList[index].0 == "contraindications" ||
        titleList[index].0 == "useRestrictions"
      {
        let contraindications =
          product.value(forKey: "contraindications") as! String
        let useRestrictions =
          product.value(forKey: "useRestrictions") as! String
        productDescription =
          (contraindications == "") ? useRestrictions :
          contraindications + "<br><br>" + useRestrictions
      } else {
        productDescription =
          product.value(forKey: titleList[index].0) as! String
      }
      sectionViewController.productDescription = productDescription
    }
  }
  
  override func viewWillTransition(
    to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    
    productTableView.reloadData()
  }
  
  deinit {
    print("*** deinit \(self)")
  }
  
  //MARK: - Other functios
  func loadData(_ newManagedObject: NSManagedObject) {
    for (attributeName, _) in  newManagedObject.entity.attributesByName {
      if let (index, title) = titlesDictionary[attributeName] {
        if let value = newManagedObject.value(forKey: attributeName) as? String
        {
          if let hasPermission = product.hasPermission {
            if value != "" {
              dictTitleList[index] = (attributeName,title)
              switch (hasPermission.boolValue, index) {
              case (_, 0...5):
                dictDetailList[index] = value
              case (false, 20):
                dictDetailList[index] = value
              default:
                break
              }
            }
          }
        }
      }
    }
    
    
    let titlesTuples = dictTitleList.sorted(by: {$0.0 < $1.0})
    for (_,(attribute,title)) in titlesTuples {
      titleList.append((attribute,title))
    }
    let detailTuples = dictDetailList.sorted(by: {$0.0 < $1.0})
    for (_, value) in detailTuples {
      detailList.append(value)
    }
  }
  
  func loadBannerView() {
    bannerView.adUnitID = "ca-app-pub-8126387378143699/8841004560"
    bannerView.delegate = self
    bannerView.rootViewController = self
    bannerView.load(GADRequest())
  }
  
  //MARK: - IBActions
  @IBAction func showMoreInformation(_ sender: UIButton) {
    let distributor =
      (product.distributor! != "") ? product.distributor! : product.lab!
    let alert =
      UIAlertController(title: "Información disponible en la siguiente" +
                               " actualización." ,
                        message: "Para mostrar este prospecto es necesaria" +
                                 " la autorización de \(distributor)",
                        preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alert.addAction(okAction)
    present(alert, animated: true, completion: nil)
  }
  
  @IBAction func unwindToProductViewController(_ segue: UIStoryboardSegue) {
    _ = navigationController?.popViewController(animated: true)
  }
}

//MARK: - UITableDataSource
extension ProductVC: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return detailList.count + 1 //for more info or other sections
  }
  
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int
  {
    if section < detailList.count {
      return 1
    } else {
      return abs(titleList.count - detailList.count)
    }
  }
  
  func tableView(_ tableView: UITableView,
                 titleForHeaderInSection section: Int) -> String? {
    if section < detailList.count {
      return titleList[section].1
    } else {
      return "Otras secciones:"
    }
  }
  
  func tableView(_ tableView: UITableView,
                 cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let hasPermission = product.hasPermission!.boolValue
    
//    if !hasPermission && indexPath.section >= detailList.count {
//      return moreInformationCell
//    } else if hasPermission &&
    if indexPath.section >= detailList.count  {
      let cellIdentifier = "OtherTitles"
      let cell: UITableViewCell! =
        tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                      for: indexPath)
      cell.textLabel?.text = titleList[detailList.count + indexPath.row].1
      
      return cell
    } else {
      let htmlString = detailList[indexPath.section]
      if htmlString.contains("<table ") {
        let cellIdentifier = "WebCell"
        let cell =
          tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                        for: indexPath) as! WebViewCell
        cell.webViewContainer.setWebViewForProspectusWithString(htmlString)
        cell.webViewContainer.indexPath = indexPath
        
        return cell
      } else {
        let cellIdentifier = "ProductDetailCell"
        let cell: UITableViewCell! =
          tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                        for: indexPath)
        let myMutableString = htmlStringToAtributtedString(htmlString )
        cell.textLabel!.attributedText = myMutableString
        
        return cell
      }
    }
  }
  
  func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView,
                 forSection section: Int)
  {
    let header: UITableViewHeaderFooterView =
      view as! UITableViewHeaderFooterView
    header.textLabel!.textColor =
      UIColor(red: 119/255, green: 170/255, blue: 17/255 , alpha: 1)
    header.textLabel!.font = UIFont.boldSystemFont(ofSize: 12.0)
  }
  
  
  func tableView(_ tableView: UITableView,
                 heightForRowAt indexPath: IndexPath) -> CGFloat
  {
    if sectionsWithRowHeight?[indexPath.section] != nil {
      return sectionsWithRowHeight![indexPath.section]!
    }
    return UITableViewAutomaticDimension
  }
}

//MARK: - UITableViewDelegate
extension ProductVC: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView,
                 didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  func tableView(_ tableView: UITableView,
                 heightForHeaderInSection section: Int) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
}

//MARK: - UIWebViewDelegate
extension ProductVC: UIWebViewDelegate {
  func webViewDidFinishLoad(_ webView: UIWebView) {
    let webContainer = webView as! HTMLTableWebView
    let section = webContainer.indexPath.section
    if sectionsWithRowHeight == nil {
      sectionsWithRowHeight = [:]
    }
    if sectionsWithRowHeight?[section] == nil {
      sectionsWithRowHeight![section] =
        webContainer.scrollView.contentSize.height
    }
  }
}

//MARK: - GADBannerViewDelegate
extension ProductVC: GADBannerViewDelegate {
  
  func adViewDidReceiveAd(_ bannerView: GADBannerView) {
    print("Banner loaded successfully")
  }
  
  func adView(_ bannerView: GADBannerView,
              didFailToReceiveAdWithError error: GADRequestError) {
    print("Fail to receive ads")
//    performSegue(withIdentifier: "ShowInfo", sender: nil)
    print(error)
  }
}
