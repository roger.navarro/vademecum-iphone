//
//  WebViewCell.swift
//  Vademecum
//
//  Created by Roger Navarro on 9/8/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit

class WebViewCell: UITableViewCell {
  
  @IBOutlet weak var webViewContainer: HTMLTableWebView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
  }
}
