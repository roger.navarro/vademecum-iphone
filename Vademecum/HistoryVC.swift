//
//  HistoryVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 8/8/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import CoreData

let historyLimit = 20

class HistoryVC: UIViewController {
  //MARK: - IBOutlets
  @IBOutlet weak var historyTableView: UITableView!
  
  //MARK: - Properties
  var managedContext: NSManagedObjectContext!
  var productsHistoryList: [Product] = []
  let historyFetchRequest =
    NSFetchRequest<NSFetchRequestResult>(entityName: "Product")
  
  //MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    historyTableView.estimatedRowHeight = 65.0;
    historyTableView.rowHeight = UITableViewAutomaticDimension;
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    let productsID = getProductsIDsFromNSUserDefaults()
    
    productsHistoryList = []
    if let productsID = productsID {
      for productID in productsID {
        historyFetchRequest.predicate =
          NSPredicate(format: "id == %@", productID)
        
        do {
          let result =
            try managedContext.fetch(historyFetchRequest) as! [Product]
          productsHistoryList.append(result.last!)
        } catch let error as NSError {
          print("Could not fetch \(error), \(error.userInfo)")
        }
      }
      productsHistoryList = productsHistoryList.reversed()
    }
    historyTableView.reloadData()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == productViewControllerSegueIdentifier {
      let productViewController = segue.destination as! ProductVC
      productViewController.managedContext = managedContext
      let indexPath = historyTableView.indexPathForSelectedRow!
      let product = productsHistoryList[indexPath.row]
      productViewController.product = product
      productViewController.mainTitleStr = product.name
    }
  }
  
}

//MARK: - UITableViewDataSource
extension HistoryVC: UITableViewDataSource {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    return productsHistoryList.count
  }
  
  func tableView(_ tableView: UITableView,
                 cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellIdentifier = "HistoryCell"
    let cell = historyTableView.dequeueReusableCell(
                 withIdentifier: cellIdentifier, for: indexPath)
    let product = productsHistoryList[indexPath.row]
    let titleLabel = cell.viewWithTag(1000) as! UILabel
    titleLabel.text = product.name!
    
    let solutionTypesLabel = cell.viewWithTag(1001) as! UILabel
    solutionTypesLabel.attributedText =
      NSAttributedString(string: product.solutionTypes!)
    
    let therapeuticCategoryLabel = cell.viewWithTag(1002) as! UILabel
    therapeuticCategoryLabel.attributedText =
      NSAttributedString(string: product.therapeuticCategory!)
    
    let labLabel = cell.viewWithTag(1003) as! UILabel
    labLabel.attributedText = NSAttributedString(string: product.lab!)
    
    return cell
  }
}

//MARK: - UITableViewDelegate
extension HistoryVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    return indexPath
  }
  
  func tableView(_ tableView: UITableView,
                 didSelectRowAt indexPath: IndexPath) {
    historyTableView.deselectRow(at: indexPath, animated: true)
  }
}
