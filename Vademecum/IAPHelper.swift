//
//  IAPHelper.swift
//  ReceiptApp
//
//  Created by Roger Navarro on 2/14/17.
//  Copyright © 2017 Roger Navarro. All rights reserved.
//

import UIKit
import StoreKit

class IAPHelper: NSObject {
  
  static let IAPHelperPurchaseNotification = "IAPHelperPurchaseNotification"
  
  typealias ProductsRequestCompletionHandler = (_ products: [SKProduct]?) -> ()
  
  fileprivate let productIdentifiers: Set<String>
  fileprivate var productsRequest: SKProductsRequest?
  fileprivate var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
  fileprivate(set) var purchasedProductIdentifiers = Set<String>()
  
  init(prodIds: Set<String>) {
    productIdentifiers = prodIds
    super.init()
    SKPaymentQueue.default().add(self)
    importIAPsFromReceipt()
  }
}

extension IAPHelper {
  func requestProducts(completionHandler: @escaping ProductsRequestCompletionHandler) {
    productsRequest?.cancel()
    productsRequestCompletionHandler = completionHandler
    
    productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
    productsRequest?.delegate = self
    productsRequest?.start()
  }
  
  func buyProduct(product: SKProduct) {
    let payment = SKPayment(product: product)
    SKPaymentQueue.default().add(payment)
  }
}

extension IAPHelper: SKProductsRequestDelegate {
  func productsRequest(_ request: SKProductsRequest,
                       didReceive response: SKProductsResponse) {
    productsRequestCompletionHandler?(response.products)
    productsRequestCompletionHandler = .none
    productsRequest = .none
  }
  
  func request(_ request: SKRequest, didFailWithError error: Error) {
    print("Error: \(error.localizedDescription)")
    productsRequestCompletionHandler?(.none)
    productsRequestCompletionHandler = .none
    productsRequest = .none
  }
}

extension IAPHelper: SKPaymentTransactionObserver {
  
  func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
    for transaction in transactions {
      switch transaction.transactionState {
      case .purchased:
        completeTransaction(transaction: transaction)
      case .failed:
        failedTransaction(transaction: transaction)
      default:
        print("Unhandled transaction state")
      }
    }
  }
  
  private func completeTransaction(transaction: SKPaymentTransaction) {
    deliverPurchaseNotificationForIdentifier(identifier: transaction.payment.productIdentifier)
    SKPaymentQueue.default().finishTransaction(transaction)
  }
  
  private func failedTransaction(transaction: SKPaymentTransaction) {
    if transaction.error != nil {
      print("Transaction error: \(transaction.error?.localizedDescription)")
    }
    SKPaymentQueue.default().finishTransaction(transaction)
  }
  
  fileprivate func deliverPurchaseNotificationForIdentifier(identifier: String?) {
    guard let identifier = identifier else { return }
    
    NotificationCenter.default
      .post(name: Notification.Name(rawValue: type(of: self).IAPHelperPurchaseNotification), object: identifier)
    purchasedProductIdentifiers.insert(identifier)
  }
}

extension IAPHelper {
  fileprivate func importIAPsFromReceipt() {
    let verifier = RMStoreAppReceiptVerifier()
    if verifier.verifyAppReceipt() {
      let iaps = verifier.appReceipt.inAppPurchases
      print(verifier.appReceipt.originalAppVersion)
      print("\n")
      if let iaps = iaps {
        for iap in iaps {
          deliverPurchaseNotificationForIdentifier(identifier: iap.productIdentifier)
        }
      }
    }
  }
}

protocol IAPContainer {
  var iapHelper : IAPHelper? { get set }
  
  func passIAPHelperToChildren()
}


extension IAPContainer where Self : UIViewController {
  func passIAPHelperToChildren() {
    for vc in childViewControllers {
      var iapContainer = vc as? IAPContainer
      iapContainer?.iapHelper = iapHelper
    }
  }
}

