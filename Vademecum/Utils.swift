//
//  Utils.swift
//  Vademecum
//
//  Created by Roger Navarro on 6/7/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import Foundation
import UIKit

// MARK: String extension
extension String {
  
  func htmlTextToString() -> String {
    let attrStr = try! NSAttributedString(
      data: self.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
      options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
      documentAttributes: nil)
    
    return attrStr.string
  }
  
  func condenseWhitespace() -> String {
    let components = self.components(separatedBy: CharacterSet.whitespacesAndNewlines)
    return components.filter { !$0.isEmpty }.joined(separator: " ")
  }
  
  func sliceFrom(_ start: String, to: String) -> String? {
    if self.contains(start) && self.contains(to) {
      return (range(of: start)?.upperBound).flatMap { sInd in
        (range(of: to, range: sInd..<endIndex)?.lowerBound).map { eInd in
          substring(with: sInd..<eInd)
        }
      }
    }
    return self
  }
}

//MARK: UI functions
func enableUISearchBarCancleButton (_ searchBar : UISearchBar) {
  for view1 in searchBar.subviews {
    for view2 in view1.subviews {
      if view2.isKind(of: UIButton.self) {
        let button = view2 as! UIButton
        button.isEnabled = true
        button.isUserInteractionEnabled = true
      }
    }
  }
}

//MARK: array functions
func uniqElementsInArray<S: Sequence, E: Hashable>(_ source: S) -> [E] where E==S.Iterator.Element {
  var seen: [E:Bool] = [:]
  return source.filter({ (v) -> Bool in
    return seen.updateValue(true, forKey: v) == nil
  })
}


//MARK: HTML string fucnticons
func trimProducts(_ record: String) -> String {
  if record.contains("</body>") {
    let str = record.sliceFrom("</p>", to: "</body>")
    return str!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
  } else {
    let str = record.replacingOccurrences(of: "</p>", with: "")
    return str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
  }
}

func htmlStringToAtributtedString(_ htmlString: String) -> NSMutableAttributedString {
  let attrStr = try! NSAttributedString(
    data: htmlString.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
    options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
    documentAttributes: nil)
  
  let myMutalbeString = NSMutableAttributedString(attributedString: attrStr)
  if #available(iOS 10.0, *) {
    myMutalbeString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 15), range: NSRange(location: 0, length: attrStr.length ))
  } else {
    myMutalbeString.addAttribute(NSFontAttributeName, value: UIFont(name: ".SFUIText-Regular", size: 15.0)!, range: NSRange(location: 0, length: attrStr.length ))
  }
  
  return myMutalbeString
}

//MARK: UIApplication Extension

extension UIApplication {
  
  class func appVersion() -> String {
    return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
  }
  
  class func appBuild() -> String {
    return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
  }
  
  class func versionBuild() -> String {
    let version = appVersion(), build = appBuild()
    
    return version == build ? "v\(version)" : "v\(version)(\(build))"
  }
}
