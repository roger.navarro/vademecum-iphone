//
//  NSUserDefaultsUtils.swift
//  Vademecum
//
//  Created by Roger Navarro on 8/8/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit

func storeProductIDInNSUserDefaults(_ productID: String) {
  let userDefaults = UserDefaults.standard
  let productIDsHistory = userDefaults.array(forKey: "productIDsHistory") as? [String]
  var idHistory: [String] = []
  
  if let history = productIDsHistory {
    idHistory = history
    if idHistory.contains(productID) {
      for i in 0 ..< idHistory.count {
        if productID == idHistory[i] {
          idHistory.remove(at: i)
          break
        }
      }
    }
    if idHistory.count == historyLimit {
      idHistory.removeFirst()
    }
  }
  idHistory.append(productID)
  userDefaults.set(idHistory, forKey: "productIDsHistory")
}

func getProductsIDsFromNSUserDefaults() -> [String]? {
  let userDefuaults = UserDefaults.standard
  let productsID = userDefuaults.array(forKey: "productIDsHistory") as? [String]
  return productsID
}
