
//  AppDelegate.swift
//  Vademecum
//
//  Created by Roger Navarro on 3/2/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  let iapHelper = IAPHelper(prodIds: Set(
    [
      ReceiptAppPurchase.NoInternetRequiredWithAdRemoval
      ].map { $0.productId }
  ))
  let currentDatabaseVersion = "v4"
  
  lazy var coreDataStack = CoreDataStack(modelName: "VadeMecumDatabase")
  
  lazy var applicationDocumentsDirectory: URL = {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "alquimia.Roger.Vademecum" in the application's documents Application Support directory.
    let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    do {
      try (urls[urls.count-1] as NSURL).setResourceValue(true, forKey:URLResourceKey.isExcludedFromBackupKey)
    } catch let error as NSError {
      print("Error excluding \(urls[urls.count-1].lastPathComponent) from backup \(error)");
    }
    return urls[urls.count-1]
  }()
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    _ = coreDataStack.mainContext
    if coreDataStack.mainContext.name == nil {
      print("There is no main context")
    }
    
    //Documents Directory
    print(applicationDocumentsDirectory.path)
    databaseUpdatingProcess()
    
    //Setup IAPHelper
//    iapHelper.requestProducts(completionHandler: {
//      products in
//      guard let products = products else {return}
//      print(products.map {$0.productIdentifier})
//    })
    
    //Setup ViewControllers and mainContext
    let loginVC = window!.rootViewController as! LoginVC
    loginVC.managedContext = coreDataStack.mainContext
//    loginVC.iapHelper = iapHelper
    
    //Setup Firebase
    FIRApp.configure()
    
    //Setup AdMobs
    GADMobileAds.configure(withApplicationID: "ca-app-pub-8126387378143699~9120206163")
    
    //Setup Google Login
    
    GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
    
    //Setup Facebook Login
    
    FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    
    //Customize UI
    
    customizeAppearance()
    
    return true
  }
  
  func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
    
    let facebookHandled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: [:])
    
    
    let googleHandled = GIDSignIn.sharedInstance().handle(url,
                                                          sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                          annotation: [:])
    
    return facebookHandled && googleHandled
  }
  
  func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    return true
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    coreDataStack.saveContext()
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    coreDataStack.saveContext()
  }
  
  func databaseUpdatingProcess() {
    let defaults = UserDefaults.standard
    let version = defaults.string(forKey: "databaseVersion")
    if version == nil {
      defaults.setValue(currentDatabaseVersion, forKey: "databaseVersion")
    } else {
      switch version! {
      case "v1":
        break
      case "v2":
        updateToDatabaseVersion3()
        updateToDatabaseVersion4(){ success in
          if success {
            defaults.setValue(currentDatabaseVersion, forKey: "databaseVersion")
          }
        }
      case "v3":
        updateToDatabaseVersion4(){ success in
          if success {
            defaults.setValue(currentDatabaseVersion, forKey: "databaseVersion")
          }
        }
        break
      case "v4":
        print("🔵 INFO: The application is already in version 4")
        break
      default:
        break
      }
    }
  }
  
  func deleteCompanyObjects() {
    let companyFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Company")
    
    do {
      let results = try coreDataStack.mainContext.fetch(companyFetchRequest) as! [Company]
      for object in results {
        coreDataStack.mainContext.delete(object)
      }
    } catch {
      print(error)
    }
  }
  
  func updateToDatabaseVersion3() {
    let productsFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Product")
    
    var predicate = NSPredicate(format: "lab == %@ OR distributor == %@", "HAHNEMANN, LABORATORIO", "HAHNEMANN, LABORATORIO")
    productsFetchRequest.predicate = predicate
    var hahnemannProducts = [Product]()
    do {
      hahnemannProducts = try coreDataStack.mainContext.fetch(productsFetchRequest) as! [Product]
    } catch let error as NSError {
      print("Could not fetch \(error), \(error.userInfo)")
    }
    
    let company: Company
    company = NSEntityDescription.insertNewObject(forEntityName: "Company", into: coreDataStack.mainContext) as! Company
    company.name = "HAHNEMANN, LABORATORIO"
    company.address = "Calle Pedro Salazar #692\nLa Paz - Bolivia"
    company.phone = "(591-2) 2415442/ 2415441 "
    company.fax = "(591-2) 2415444"
    company.email = "info@labhahnemann.com"
    company.website = "www.labhahnemann.com"
    company.hasPermission = true
    let image = UIImage(named: "hahnemann.png")
    company.image = UIImagePNGRepresentation(image!) as NSData?
    for product in hahnemannProducts {
      product.hasPermission = true
      company.addToCompanyProducts(product)
    }
    
    predicate = NSPredicate(format: "lab == %@ OR distributor == %@", "IMBOLMED", "IMBOLMED")
    productsFetchRequest.predicate = predicate
    var imbolmedProducts = [Product]()
    do {
      imbolmedProducts = try coreDataStack.mainContext.fetch(productsFetchRequest) as! [Product]
    } catch let error as NSError {
      print("Could not fetch \(error), \(error.userInfo)")
    }
    let company2: Company
    company2 = NSEntityDescription.insertNewObject(forEntityName: "Company", into: coreDataStack.mainContext) as! Company
    company2.name = "IMBOLMED"
    company2.address = "Calle Salta N° 340\nZona Sur\nLa Paz - Bolivia"
    company2.phone = "(591-2) 2795134"
    company2.fax = nil
    company2.email = "info@imbolmed.com"
    company2.website = "www.imbolmed.com"
    company2.hasPermission = true
    let image2 = UIImage(named: "imbolmed.png")
    company2.image = UIImagePNGRepresentation(image2!) as NSData?
    for product in imbolmedProducts {
      product.hasPermission = true
      company2.addToCompanyProducts(product)
    }
    
    do {
      try coreDataStack.mainContext.save()
    } catch {
      print(error)
    }
  }
  
  typealias CompletionHandler = (_ success:Bool) -> Void
  
  func updateToDatabaseVersion4(completionHandler: CompletionHandler) {
    deleteCompanyObjects() //remove imbolmed and hahnemann to upload csv
    let dataImporter = DataImporter(managedContext: coreDataStack.mainContext)
    dataImporter.loadCompanyData()
    joinProductsWithCompanies(managedContext: coreDataStack.mainContext)
    completionHandler(true)
  }
  
  func customizeAppearance() {
    let vademecumGreen = UIColor(red: 119/255,
                                 green: 170/255,
                                 blue: 17/255,
                                 alpha: 1)
    UINavigationBar.appearance().barTintColor = vademecumGreen
    UINavigationBar.appearance().titleTextAttributes = [
      NSForegroundColorAttributeName: UIColor.white ]
  }
}
