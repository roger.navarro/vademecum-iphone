//
//  LoginPasswordVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 1/18/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import UIKit
import Firebase

class LoginPasswordVC: UIViewController {
  
  //MARK: IBOutlets
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  //MARK: Properties
  var currentTextField: UITextField?
  
  //MARK: View Life Cycle
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    registerForKeyboardNotifications()
    activityIndicator.isHidden = true
  }
  
  deinit {
    deregisterFromKeyboardNotifications()
    print("deinit")
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  //MARK: IBActions
  @IBAction func loginUser(_ sender: UIButton) {
    currentTextField?.resignFirstResponder()
    let email = emailTextField.text!
    let password = passwordTextField.text!
    
    startActivityIndicator()
    FIRAuth.auth()?.signIn(withEmail: email, password: password)
    { (user, error) in
      
      if let error = error {
        self.showAlertWhileLoginUser(title: nil, msg: error.localizedDescription)
        return
      }
      if let user = user, user.isEmailVerified {
        self.dismiss(animated: true, completion: nil)
      } else {
        self.showAlertWhileLoginUser(title: nil, msg:
          "Aún no se ha confirmado el correo de verificación")
      }
    }
  }
  
  @IBAction func resetPass(_ sender: Any) {
    currentTextField?.resignFirstResponder()
    let email = emailTextField.text!
    
    startActivityIndicator()
    if email.isEmpty {
      var message = "Por favor ingrese el correo"
      message += " electrónico asociado de la cuenta que desea resetear"
      self.showAlertWhileLoginUser(title: nil, msg: message)
      return
    }
    FIRAuth.auth()?.sendPasswordReset(withEmail: email, completion: { error in
      if let error = error {
        self.showAlertWhileLoginUser(title: nil, msg: error.localizedDescription)
        return
      }
      self.showAlertWhileLoginUser(title: "Información", msg:
        "Se le ha enviado un correo para continuar con el cambio de contraseña")
    })
  }
  
  @IBAction func close(_ sender: Any) {
    currentTextField?.resignFirstResponder()
    dismiss(animated: true, completion: nil)
  }
  
  
  //MARK: Other functions
  
  func showAlertWhileLoginUser(title: String?, msg: String) {
    let alert = UIAlertController(title: title ?? "Error",
                                  message: msg,
                                  preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default,
                                 handler: { _ in self.stopActivityIndicator()})
    alert.addAction(okAction)
    present(alert, animated: true, completion: nil)
  }
  
  func startActivityIndicator() {
    activityIndicator.isHidden = false
    activityIndicator.startAnimating()
  }
  
  func stopActivityIndicator() {
    activityIndicator.stopAnimating()
    activityIndicator.isHidden = true
  }
  
}

//MARK: - TextFieldDelegate
extension LoginPasswordVC: UITextFieldDelegate {
  
  func registerForKeyboardNotifications(){
    //Adding notifies on keyboard appearing
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWasShown(notification:)),
      name: NSNotification.Name.UIKeyboardWillShow,
      object: nil
    )
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillBeHidden(notification:)),
      name: NSNotification.Name.UIKeyboardWillHide,
      object: nil
    )
  }
  
  func deregisterFromKeyboardNotifications(){
    //Removing notifies on keyboard appearing
    NotificationCenter.default.removeObserver(
      self,
      name: NSNotification.Name.UIKeyboardWillShow,
      object: nil
    )
    NotificationCenter.default.removeObserver(
      self,
      name: NSNotification.Name.UIKeyboardWillHide,
      object: nil
    )
  }
  
  func keyboardWasShown(notification: NSNotification){
    self.scrollView.isScrollEnabled = true
    var info = notification.userInfo!
    let keyboardSize =
      (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets =
      UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
    
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    
    var aRect : CGRect = self.view.frame
    aRect.size.height -= keyboardSize!.height
    if let activeField = self.currentTextField {
      if (!aRect.contains(activeField.frame.origin)){
        self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
      }
    }
  }
  
  func keyboardWillBeHidden(notification: NSNotification){
    //Once keyboard disappears, restore original positions
    var info = notification.userInfo!
    let keyboardSize =
      (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets =
      UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    self.view.endEditing(true)
    self.scrollView.isScrollEnabled = false
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField){
    currentTextField = textField
  }
  
  func textFieldDidEndEditing(_ textField: UITextField){
    currentTextField = nil
  }
}

