//
//  NoInternetConnectionVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 3/8/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import UIKit

class NoInternetConecctionVC: UIViewController {
  
  //MARK: - View life cycle
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    modalPresentationStyle = .custom
    transitioningDelegate = self
  }
}

//MARK: - UIViewControllerTransitioningDelegate

extension NoInternetConecctionVC: UIViewControllerTransitioningDelegate {
  func presentationController(forPresented presented: UIViewController,
                              presenting: UIViewController?,
                              source: UIViewController)
    -> UIPresentationController?
  {
    return DimmingPresentationController(presentedViewController: presented,
                                         presenting: presenting)
  }
}
