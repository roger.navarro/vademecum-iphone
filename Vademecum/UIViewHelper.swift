//
//  UIViewHelper.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/10/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import UIKit

class UIViewHelper {
  func addConstraints(fromView newView: UIView, toView view: UIView) {
    newView.frame = CGRect(x: 0,
                           y: 0,
                           width: view.frame.size.width,
                           height: view.frame.size.height)
    newView.translatesAutoresizingMaskIntoConstraints = false
    let left = NSLayoutConstraint(item: newView,
                                  attribute: .leading,
                                  relatedBy: .equal,
                                  toItem: view,
                                  attribute: .leading,
                                  multiplier: 1,
                                  constant: 0)
    let right = NSLayoutConstraint(item: newView,
                                   attribute: .trailing,
                                   relatedBy: .equal,
                                   toItem: view,
                                   attribute: .trailing ,
                                   multiplier: 1,
                                   constant: 0)
    let bottom = NSLayoutConstraint(item: newView,
                                    attribute: .bottom,
                                    relatedBy: .equal,
                                    toItem: view,
                                    attribute: .bottom ,
                                    multiplier: 1,
                                    constant: 0)
    let top = NSLayoutConstraint(item: newView,
                                 attribute: .top,
                                 relatedBy: .equal,
                                 toItem: view,
                                 attribute: .top,
                                 multiplier: 1,
                                 constant: 0)
    top.priority = 999
    bottom.priority = 999
    view.addConstraints([left,right,bottom,top])
  }
}
