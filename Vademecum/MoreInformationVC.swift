//
//  MoreInformationVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 9/15/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import CoreData
import Firebase

class MoreInformationVC: UITableViewController {
  
  //MARK: - IBOutlets
  @IBOutlet weak var logOutSessionCell: UITableViewCell!
  
  //MARK: - Properties
  var managedContext: NSManagedObjectContext!
  var firebaseCurrentUser: FIRUser?
  
  //MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    firebaseCurrentUser = FIRAuth.auth()?.currentUser
    if firebaseCurrentUser == nil {
      logOutSessionCell.imageView?.image = nil
      
      let fontSize = UIFont.systemFontSize
      
      let attrs = [
        NSFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize),
        NSForegroundColorAttributeName: UIColor.black
      ]
      let attrStr = NSMutableAttributedString(string: "Iniciar sesión",
                                              attributes: attrs)
      
      logOutSessionCell.textLabel?.attributedText = attrStr
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowHistory" {
      let historyViewController = segue.destination as! HistoryVC
      historyViewController.managedContext = managedContext
   }
  }
  
  override func shouldPerformSegue(withIdentifier identifier: String,
                                   sender: Any?) -> Bool {
    if firebaseCurrentUser == nil {
      if identifier == "ShowLogOut" {
        return false
      }
    }
    return true
  }
  
  //MARK: - TableViewDelegate
  
  override func tableView(_ tableView: UITableView,
                          didSelectRowAt indexPath: IndexPath) {
    if indexPath.row == 2 {
      if firebaseCurrentUser == nil {
        performSegue(withIdentifier: "unwindToLoginViewController", sender: nil)
      }
    }
  }
}
