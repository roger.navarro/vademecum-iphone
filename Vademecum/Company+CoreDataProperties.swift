//
//  Company+CoreDataProperties.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/18/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import Foundation
import CoreData


extension Company {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Company> {
        return NSFetchRequest<Company>(entityName: "Company")
    }

    @NSManaged public var address: String?
    @NSManaged public var email: String?
    @NSManaged public var fax: String?
    @NSManaged public var hasPermission: NSNumber
    @NSManaged public var image: NSData?
    @NSManaged public var name: String
    @NSManaged public var phone: String?
    @NSManaged public var shortName: String?
    @NSManaged public var website: String?
    @NSManaged public var companyProducts: NSSet?
    @NSManaged public var productsAsLaboratory: NSSet?

}

// MARK: Generated accessors for companyProducts
extension Company {

    @objc(addCompanyProductsObject:)
    @NSManaged public func addToCompanyProducts(_ value: Product)

    @objc(removeCompanyProductsObject:)
    @NSManaged public func removeFromCompanyProducts(_ value: Product)

    @objc(addCompanyProducts:)
    @NSManaged public func addToCompanyProducts(_ values: NSSet)

    @objc(removeCompanyProducts:)
    @NSManaged public func removeFromCompanyProducts(_ values: NSSet)

}

// MARK: Generated accessors for productsAsLaboratory
extension Company {

    @objc(addProductsAsLaboratoryObject:)
    @NSManaged public func addToProductsAsLaboratory(_ value: Product)

    @objc(removeProductsAsLaboratoryObject:)
    @NSManaged public func removeFromProductsAsLaboratory(_ value: Product)

    @objc(addProductsAsLaboratory:)
    @NSManaged public func addToProductsAsLaboratory(_ values: NSSet)

    @objc(removeProductsAsLaboratory:)
    @NSManaged public func removeFromProductsAsLaboratory(_ values: NSSet)

}
