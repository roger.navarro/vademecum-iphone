//
//  DataImporter.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/13/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//
import CoreData
import UIKit

class DataImporter {
  
  var managedContext: NSManagedObjectContext!
  
  init (managedContext: NSManagedObjectContext) {
    self.managedContext = managedContext
  }
  
  func loadCompanyData() {
    if let contentsOfURL =
      Bundle.main.url(forResource: "companies", withExtension: "csv") {
      var error: NSError?
      parseCSVofCompanies(contentsOfURL: contentsOfURL,
                          encoding: String.Encoding.utf8,
                          error: &error)
    }
  }
  
  func parseCSVofCompanies(contentsOfURL: URL,
                           encoding: String.Encoding,
                           error: NSErrorPointer) {
    do {
      let data = try Data.init(contentsOf: contentsOfURL)
      if let content = String(data: data, encoding: encoding) {
        var lines = content.components(separatedBy: "\r\n") as [String]
        lines.removeLast()//Removes unnecesary line
        guard let companyEntity =
          NSEntityDescription.entity(forEntityName: "Company",
                                     in: managedContext) else { return }
        for line in lines {
          var fields = [String]()
          
          if line.range(of:"\"") != nil {
            var textToScan:String = line
            var value:NSString?
            var textScanner:Scanner = Scanner(string: textToScan)
            while textScanner.string != "" {
              
              if (textScanner.string as NSString).substring(to: 1) == "\"" {
                textScanner.scanLocation += 1
                textScanner.scanUpTo("\"", into: &value)
                textScanner.scanLocation += 1
              } else {
                textScanner.scanUpTo(",", into: &value)
              }
              
              // Store the value into the values array
              fields.append(value! as String)
              
              // Retrieve the unscanned remainder of the string
              if textScanner.scanLocation < textScanner.string.characters.count {
                textToScan = (textScanner.string as NSString).substring(from: textScanner.scanLocation + 1)
                value = ""
              } else {
                textToScan = ""
                value = ""
              }
              textScanner = Scanner(string: textToScan)
            }
            
            // For a line without double quotes, we can simply separate the string
            // by using the delimiter (e.g. comma)
          } else  {
            fields = line.components(separatedBy: ",")
          }
          
          
          let company = Company(entity: companyEntity,
                                insertInto: managedContext)
          if !fields[0].isEmpty {
            company.name = fields[0]
          }
          if !fields[1].isEmpty {
            company.address = replaceMatches("\\\\n", inString: fields[1], withString: "\n")
          }
          if !fields[2].isEmpty {
            company.phone = fields[2]
          }
          if !fields[3].isEmpty {
            company.fax = fields[3]
          }
          if !fields[4].isEmpty {
            company.email = fields[4]
          }
          if !fields[5].isEmpty {
            company.website = fields[5]
          }
          if !fields[6].isEmpty {
            company.shortName = fields[6]
          }
          let image = UIImage(named: "\(fields[0]).png") // The file must be in the project's files.
          if let image = image {
            let nsData = UIImagePNGRepresentation(image)! as NSData
            company.image = ImageUtils.imageDataScaledToHeight(
              nsData as Data,height: 120) as NSData?
          } else {
            company.image = UIImagePNGRepresentation(UIImage.init(named: "company-1")!)! as NSData
            print("The image wans't found")
          }
          company.hasPermission = true
          do {
            try managedContext.save()
          } catch {
            fatalError(
              "unresolved error \(error), \(error.localizedDescription)")
          }
        }
        print("Imported \(lines.count) companies")
      } else {
        print("Warning: There is no content in the companies.csv")
      }
    } catch {
      fatalError(
        "Error while loding data: \(error), \(error.localizedDescription)")
    }
  }
}
