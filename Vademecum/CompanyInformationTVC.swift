//
//  CompanyInformationTVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 9/22/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit

class CompanyInformationTVC: UITableViewController {
  
  //MARK: - IBOutlets
  @IBOutlet weak var companyImage: UIImageView!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var address: UILabel!
  @IBOutlet weak var phone: UILabel!
  @IBOutlet weak var fax: UILabel!
  @IBOutlet weak var email: UILabel!
  @IBOutlet weak var webSite: UILabel!
  
  //MARK: - Properties
  var company: Company!
  
  //MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    loadData()
  }
  
  //MARK: - Other functions
  func loadData() {
    name.text = company.name
    if let image = company.image {
      companyImage.image = UIImage(data: image as Data)
    }
    
    if let address = company.address {
      self.address.text = address
    } else { self.address.text = ""}
    
    if let phone = company.phone {
      self.phone.text = phone
    } else { self.phone.text = ""}
    
    if let fax = company.fax {
      self.fax.text = fax
    } else { self.fax.text = ""}
    
    if let email = company.email {
      self.email.text = email
    } else { self.email.text = ""}
    
    if let webSite = company.website {
      self.webSite.text = webSite
    } else { self.webSite.text = ""}
  }
}

//MARK - Table view data source
extension CompanyInformationTVC {
  override func tableView(_ tableView: UITableView,
                          numberOfRowsInSection section: Int) -> Int {
    return 6
  }
  
  override func tableView(_ tableView: UITableView,
                          heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.row == 0 {
      return 131
    } else if indexPath.row == 1 {
      address.frame.size = CGSize(width: view.bounds.size.width - 115,
                                  height: 10000)
      address.sizeToFit()
      address.frame.origin.x =
        view.bounds.size.width - address.frame.size.width - 15
      return address.frame.size.height + 16
    } else {
      return 44
    }
  }
}
