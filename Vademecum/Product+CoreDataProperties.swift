//
//  Product+CoreDataProperties.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/18/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import Foundation
import CoreData


extension Product {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Product> {
        return NSFetchRequest<Product>(entityName: "Product")
    }

    @NSManaged public var adverseReactions: String?
    @NSManaged public var contraindications: String?
    @NSManaged public var distributor: String?
    @NSManaged public var dosageAdministration: String?
    @NSManaged public var dosageForm: String?
    @NSManaged public var drugInteractions: String?
    @NSManaged public var generalPrecautions: String?
    @NSManaged public var genesis: String?
    @NSManaged public var hasPermission: NSNumber?
    @NSManaged public var healthRecord: String?
    @NSManaged public var humanPharmacokinetics: String?
    @NSManaged public var id: String?
    @NSManaged public var isGeneric: String?
    @NSManaged public var isVisible: NSNumber?
    @NSManaged public var lab: String?
    @NSManaged public var labAddress: String?
    @NSManaged public var labAlterations: String?
    @NSManaged public var name: String?
    @NSManaged public var overdoseManagement: String?
    @NSManaged public var presentations: String?
    @NSManaged public var productDescription: String?
    @NSManaged public var protectionLegends: String?
    @NSManaged public var solutionTypes: String?
    @NSManaged public var storageRecomendations: String?
    @NSManaged public var therapeuticCategory: String?
    @NSManaged public var therapeuticIndications: String?
    @NSManaged public var useRestrictions: String?
    @NSManaged public var activePrincipleList: NSSet?
    @NSManaged public var company: Company?
    @NSManaged public var laboratory: Company?

}

// MARK: Generated accessors for activePrincipleList
extension Product {

    @objc(addActivePrincipleListObject:)
    @NSManaged public func addToActivePrincipleList(_ value: ActivePrinciple)

    @objc(removeActivePrincipleListObject:)
    @NSManaged public func removeFromActivePrincipleList(_ value: ActivePrinciple)

    @objc(addActivePrincipleList:)
    @NSManaged public func addToActivePrincipleList(_ values: NSSet)

    @objc(removeActivePrincipleList:)
    @NSManaged public func removeFromActivePrincipleList(_ values: NSSet)

}
