//
//  SearchVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/10/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import UIKit
import CoreData

class SearchVC: UIViewController {
  
  //MARK: - IBOulets
  
  @IBOutlet weak var segmentControl: UISegmentedControl!
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var productsView: UIView!
  @IBOutlet weak var activePrincipleView: UIView!
  @IBOutlet weak var searchBar: UISearchBar!
  
  //MARK: - Properties
  
  var managedContext: NSManagedObjectContext!
  var productsVC: ProductsVC!
  var activePrincipleVC: ActivePrincipleVC!
  
  enum TableType {
    case products
    case activePrinciple
  }
  
  //MARK: - View Life Cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    contentView.addSubview(productsView)
    UIViewHelper().addConstraints(fromView: productsView, toView: contentView)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    switch segue.identifier! {
    case "ShowProductsNavigationController":
      productsVC = segue.destination as! ProductsVC
      productsVC.managedContext = managedContext
      searchBar.delegate = productsVC
    case "ShowActivePrincipleNavigationController":
      activePrincipleVC = segue.destination as! ActivePrincipleVC
      activePrincipleVC.managedContext = managedContext
    default:
      break
    }
  }
  
  //MARK: - IBActions
  
  @IBAction func segmentChanged(_ sender: UISegmentedControl) {
    switch sender.selectedSegmentIndex {
    case 0:
      removeTable(.products)
      contentView.addSubview(productsView)
      UIViewHelper().addConstraints(fromView: productsView, toView: contentView)
      resignFirstResponder()
      if searchBar.isFirstResponder {
        searchBar.placeholder = "Ej. Digestan"
      } else {
        searchBar.placeholder = "Buscar productos..."
      }
      searchBar.delegate = productsVC
      if let text = searchBar.text {
        productsVC.searchText(text)
      }
    case 1:
      removeTable(.activePrinciple)
      contentView.addSubview(activePrincipleView)
      UIViewHelper().addConstraints(fromView: activePrincipleView,
                                    toView: contentView)
      resignFirstResponder()
      searchBar.placeholder = "Buscar principio activo..."
      if searchBar.isFirstResponder {
        searchBar.placeholder = "Ej. Paracetamol o Ibuprofeno"
      } else {
        searchBar.placeholder = "Buscar principio activo..."
      }
      searchBar.delegate = activePrincipleVC
      if let text = searchBar.text {
        activePrincipleVC.searchText(text)
      }
    default:
      break
    }
  }
  
  //MARK: - Other functions
  
  func removeTable(_ tableType: TableType) {
    switch tableType {
    case .products:
      for view in contentView.subviews {
        if view.tag == 1000 { //productsTable
          view.removeFromSuperview()
          break
        }
      }
    case .activePrinciple:
      for view in contentView.subviews {
        if view.tag == 100 {//informationTable
          view.removeFromSuperview()
          break
        }
      }
    }
  }
}
