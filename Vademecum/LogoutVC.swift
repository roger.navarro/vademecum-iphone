//
//  LogoutVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 4/4/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import UIKit
import Firebase

class LogoutVC: UIViewController {
  
  //MARK: - IBOutlets
  
  @IBOutlet weak var profilePicture: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  
  //MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let currentUser = FIRAuth.auth()?.currentUser
    
    if let photoURL = currentUser?.photoURL {
      DispatchQueue.global().async {
        let data = try? Data.init(contentsOf: photoURL)
        DispatchQueue.main.async {
          if let data = data {
            self.profilePicture.image = UIImage(data: data)
            self.profilePicture.layer.cornerRadius =
              (self.profilePicture.image?.size.width)! / 2
            self.profilePicture.clipsToBounds = true
          }
        }
      }
    }
    
    
    if let userName = FIRAuth.auth()?.currentUser?.displayName {
      nameLabel.text = userName
    } else {
      nameLabel.text = "Usuario"
    }
    
    let defaults = UserDefaults.standard
    let emailAddress = defaults.string(forKey: "justLoggedInEmail")
    if let email = emailAddress {
      emailLabel.text = email
    } else {
      emailLabel.text = ""
    }
    
  }
  
  @IBAction func logOut(_ sender: Any) {
    performSegue(withIdentifier: "unwindToLoginViewController", sender: nil)
    let defaults = UserDefaults.standard
    defaults.set(nil, forKey: "justLoggedInEmail")
  }
  
}
