//
//  Company+CoreDataClass.swift
//  Vademecum
//
//  Created by Roger Navarro on 11/17/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import Foundation
import CoreData


public class Company: NSManagedObject {

}

extension Company {
  static func getCompanyBy(name: String, in managedContext: NSManagedObjectContext) -> Company? {
    guard !name.isEmpty else {
      return nil
    }
    
    let companyFetchRequest = NSFetchRequest<Company>(entityName: "Company")
    let predicate = NSPredicate(format: "name == %@", name)
    companyFetchRequest.predicate = predicate
    let companyList: [Company]!
    do {
      companyList = try managedContext.fetch(companyFetchRequest)
      guard companyList.count > 0, companyList.count <= 1 else {
        return nil
      }
      return companyList[0]
    } catch {
      fatalError("🔴Error: \(error), \(error.localizedDescription)")
    }
  }
}
