//
//  HTMLTableWebView.swift
//  Vademecum
//
//  Created by Roger Navarro on 9/8/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit

class HTMLTableWebView: UIWebView {
  
  var indexPath = IndexPath()
  
  func setWebViewForProspectusWithString(_ htmlString: String) {
    var htmlHead = "<html><head><style>table, td, th {border: 1px solid #ddd;text-align: left;}table {border-collapse: collapse; width: 100%;}th, td {text-align: left;padding: 8px;font-size: 16px;font-family: -apple-system, Helvetica, Arial, sans-serif;}th {background-color: #77aa11;color: white;}body{font-size: 15px;font-family: -apple-system, Helvetica, Arial, sans-serif;} table{margin-top:10px;margin-bottom:10px; border:0;} .emptycell{visibility: hidden;} .borderoff{border-top: 1px solid #FFFFFF !important;border-left: 1px solid #FFFFFF !important;border-right: 1px solid #FFFFFF !important; padding-top:8px; border-bottom: 1px solid #ddd !important;}</style></head><body>!@#BODY!@#</body></html>"
    let htmlBody = htmlString
    htmlHead = htmlHead.replacingOccurrences(of: "!@#BODY!@#", with: htmlBody)
    htmlHead = htmlHead.replacingOccurrences(of: "<td colspan", with: "<td class=\"borderoff\" colspan")
    htmlHead = htmlHead.replacingOccurrences(of: "></td>", with: " class=\"emptycell\"></td>")
    self.loadHTMLString(htmlHead, baseURL: nil)
    self.scrollView.bounces = false
  }
}

