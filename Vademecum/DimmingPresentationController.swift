//
//  DimmingPresentationController.swift
//  Vademecum
//
//  Created by Roger Navarro on 3/8/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import UIKit

class DimmingPresentationController: UIPresentationController {
  override var shouldRemovePresentersView: Bool {
    return false
  }
}
