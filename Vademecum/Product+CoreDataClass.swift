//
//  Product+CoreDataClass.swift
//  Vademecum
//
//  Created by Roger Navarro on 11/17/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import Foundation
import CoreData

@objc(Product)
public class Product: NSManagedObject {

}

extension Product {
  static func getProductBy(id: String, in managedContext: NSManagedObjectContext)
    -> Product? {
    let productFetchRequest = NSFetchRequest<Product>(entityName: "Product")
    let predicate = NSPredicate(format: "id == %@", id)
    productFetchRequest.predicate = predicate
    let product: [Product]
    
    do {
      product = try managedContext.fetch(productFetchRequest)
      guard product.count <= 1 else {
        return nil
      }
      return product[0]
    } catch {
      fatalError("🔴 Error retring product by ID" +
        ", \(error), \(error.localizedDescription)")
    }
    
  }
}
