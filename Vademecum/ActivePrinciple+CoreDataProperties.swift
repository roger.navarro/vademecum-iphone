//
//  ActivePrinciple+CoreDataProperties.swift
//  Vademecum
//
//  Created by Roger Navarro on 11/17/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import Foundation
import CoreData


extension ActivePrinciple {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ActivePrinciple> {
        return NSFetchRequest<ActivePrinciple>(entityName: "ActivePrinciple");
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var productList: NSSet?

}

// MARK: Generated accessors for productList
extension ActivePrinciple {

    @objc(addProductListObject:)
    @NSManaged public func addToProductList(_ value: Product)

    @objc(removeProductListObject:)
    @NSManaged public func removeFromProductList(_ value: Product)

    @objc(addProductList:)
    @NSManaged public func addToProductList(_ values: NSSet)

    @objc(removeProductList:)
    @NSManaged public func removeFromProductList(_ values: NSSet)

}
