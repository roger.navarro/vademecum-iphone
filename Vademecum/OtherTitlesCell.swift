//
//  OtherTitlesCell.swift
//  Vademecum
//
//  Created by Roger Navarro on 9/8/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit

class OtherTitlesCell: UITableViewCell {
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    let context: CGContext = UIGraphicsGetCurrentContext()!
    context.beginPath()
    
    
    context.setStrokeColor(UIColor.lightGray.cgColor)
    context.setLineWidth(1)
    
    // Top line
    context.move(to: CGPoint(x: 16, y: self.bounds.size.height))
    context.addLine(to: CGPoint(x: self.bounds.size.width, y: self.bounds.size.height))
    
    context.drawPath(using: .stroke)
  }
  
}
