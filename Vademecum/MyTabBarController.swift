//
//  MyTabBarController.swift
//  Vademecum
//
//  Created by Roger Navarro on 9/27/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit

class MyTabBarController: UITabBarController {
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent
  }
  
  override var childViewControllerForStatusBarStyle : UIViewController? {
    return nil
  }
}
