//
//  DescriptionVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 9/8/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit
import GoogleMobileAds

class DescriptionVC: UIViewController {
  
  //MARK: - IBOutlets
  @IBOutlet weak var sectionLabel: UILabel!
  @IBOutlet weak var descriptionText: UILabel!
  @IBOutlet weak var webDescription: HTMLTableWebView!
  
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var bannerView: GADBannerView!
  
  //MARK: - Properties
  var productDescription: String!
  var productDescriptionTitle: String!
  
  var productName: String! {
    didSet {
      self.title = productName
    }
  }
  
  //MARK: - View life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.loadSectionTitle()
    self.loadSectionDescription()
    self.loadBannerView()
  }
  
  deinit {
    print("*** deinit \(self)")
  }
  
  override func viewWillTransition(
    to size: CGSize,
    with coordinator: UIViewControllerTransitionCoordinator)
  {
    if !webDescription.isHidden {
      webDescription.setWebViewForProspectusWithString(productDescription)
    }
  }
  
  //MARK: - Other functions
  func loadBannerView() {
    bannerView.adUnitID = "ca-app-pub-8126387378143699/8841004560"
    bannerView.delegate = self
    bannerView.rootViewController = self
    bannerView.load(GADRequest())
  }
}

//MARK: Oulets Loaders
extension DescriptionVC {
  func loadSectionDescription() {
    if productDescription.contains("<table ") {
      descriptionText.isHidden = true
      webDescription.setWebViewForProspectusWithString(productDescription)
    } else {
      webDescription.isHidden = true
      descriptionText.attributedText =
        htmlStringToAtributtedString(productDescription)
    }
  }
  
  func loadSectionTitle() {
    sectionLabel.text = productDescriptionTitle
  }
}

extension DescriptionVC: UIWebViewDelegate {
  func webViewDidFinishLoad(_ webView: UIWebView) {
    webView.frame = CGRect(x: 8.0,
                           y: 34,
                           width: contentView.frame.size.width - 8.0,
                           height: webView.scrollView.contentSize.height)
    let height = webView.scrollView.contentSize.height
    let size = CGSize(width: contentView.frame.size.width,
                      height: height + sectionLabel.frame.size.height)
    scrollView.contentSize = size
  }
}

//MARK: - GADBannerViewDelegate
extension DescriptionVC: GADBannerViewDelegate {
  
  func adViewDidReceiveAd(_ bannerView: GADBannerView) {
    print("Banner loaded successfully")
  }
  
  func adView(_ bannerView: GADBannerView,
              didFailToReceiveAdWithError error: GADRequestError) {
    print("Fail to receive ads")
    print(error)
  }
}
