//
//  RegexUtilities.swift
//  Vademecum
//
//  Created by Roger Navarro on 7/29/16.
//  Copyright © 2016 Alquimia. All rights reserved.
//

import UIKit

func highlightMatches(_ pattern: String,
                      inString string: String) -> NSAttributedString {
  
  let encoding = String.Encoding.ascii
  let dataEncodedString =
    string.data(using: encoding, allowLossyConversion: true)!
  let noDiacriticsString = String.init(data: dataEncodedString,
                                       encoding: encoding)
  let dataEncodedPattern = pattern.data(using: encoding,
                                        allowLossyConversion: true)!
  let noDiacriticsPattern = String.init(data: dataEncodedPattern,
                                        encoding: encoding)

  let regex = try! NSRegularExpression(pattern: (noDiacriticsPattern ?? pattern),
                                        options: [.caseInsensitive,
                                                  .ignoreMetacharacters])

  let range = NSMakeRange(0, string.characters.count)
  let matches = regex.matches(in: (noDiacriticsString ?? string) ,
                              options: [], range: range)
  
  
  let attributedText = NSMutableAttributedString(string: string)
  
  for match in matches {
    attributedText.addAttribute(
      NSBackgroundColorAttributeName,
      value: UIColor.init(red: 230/255,
                          green: 230/255,
                          blue: 230/255,
                          alpha: 1),
      range: match.range)
  }
  
  return attributedText.copy() as! NSAttributedString
}

func listMatches(_ pattern: String, inString string: String) -> [String] {
  let regex = try! NSRegularExpression(pattern: pattern, options: [])
  let range = NSMakeRange(0,string.characters.count)
  let matches = regex.matches(in: string, options: [], range: range)
  
  return matches.map {
    let range = $0.range
    return (string as NSString).substring(with: range)
  }
}

func listGroups(_ pattern: String, inString string: String) -> [String] {
  let regex = try! NSRegularExpression(pattern: pattern, options: [])
  let range = NSMakeRange(0, string.characters.count)
  let matches = regex.matches(in: string, options: [], range: range)
  
  var groupMatches = [String]()
  for match in matches {
    let rangeCount = match.numberOfRanges
    
    for group in 0..<rangeCount {
      groupMatches.append((string as NSString).substring(with: match.rangeAt(group)))
    }
  }
  
  return groupMatches
}

func containsMatch(_ pattern: String, inString string: String) -> Bool {
  let regex = try! NSRegularExpression(pattern: pattern, options: [])
  let range = NSMakeRange(0, string.characters.count)
  return regex.firstMatch(in: string, options: [], range: range) != nil
}

func replaceMatches(_ pattern: String, inString string: String, withString replacementString: String) -> String? {
  let regex = try! NSRegularExpression(pattern: pattern, options: [])
  let range = NSMakeRange(0, string.characters.count)
  return regex.stringByReplacingMatches(in: string, options: [], range: range, withTemplate: replacementString)
}
