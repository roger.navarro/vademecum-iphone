//
//  CreateUserVC.swift
//  Vademecum
//
//  Created by Roger Navarro on 1/18/17.
//  Copyright © 2017 Alquimia. All rights reserved.
//

import UIKit
import Firebase

class CreateUserVC: UIViewController {
  
  //MARK: IBOutlets
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  //MARK: Properties
  var currentTextField: UITextField?
  
  //MARK: View Life Cycle
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    registerForKeyboardNotifications()
    activityIndicator.isHidden = true
  }
  
  deinit {
    deregisterFromKeyboardNotifications()
    print("deinit")
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  //MARK: IBActions
  @IBAction func createNewUser(_ sender: UIButton) {
    currentTextField?.resignFirstResponder()
    let email = emailTextField.text!
    let password = passwordTextField.text!
    
    activityIndicator.isHidden = false
    activityIndicator.startAnimating()
    FIRAuth.auth()?.createUser(withEmail: email, password: password)
    { (user, error) in
      
      if let error = error {
        self.showAlertWhileCreatingUser(msg: error.localizedDescription)
        return
      }
      if let user = user {
        user.sendEmailVerification(completion: { error in
          if let error = error {
            self.showAlertWhileCreatingUser(msg: error.localizedDescription)
            return
          } else {
            self.showAlerMessageAfterSendingVerificationEmail(email: email)
          }
        })
      }
    }
  }
  
  
  @IBAction func close(_ sender: Any) {
    currentTextField?.resignFirstResponder()
    dismiss(animated: true, completion: nil)
  }
  
  //MARK: Other functions
  
  func showAlerMessageAfterSendingVerificationEmail(email: String) {
    let alert = UIAlertController(
      title: "Se ha enviado un correo electrónico de confirmación a:",
      message: email,
      preferredStyle: .alert)
    
    let okAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
      self.stopActivityIndicator()
      self.dismiss(animated: true, completion:  nil)
    })
    alert.addAction(okAction)
    present(alert, animated: true, completion: nil)
  }
  
  func showAlertWhileCreatingUser(msg: String) {
    let alert = UIAlertController(title: "Error",
                                  message: msg,
                                  preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default,
                                 handler: { _ in self.stopActivityIndicator()})
    alert.addAction(okAction)
    present(alert, animated: true, completion: nil)
  }
  
  func stopActivityIndicator() {
    activityIndicator.stopAnimating()
    activityIndicator.isHidden = true
  }
  
}

//MARK: - TextFieldDelegate

extension CreateUserVC: UITextFieldDelegate {
  
  func registerForKeyboardNotifications(){
    //Adding notifies on keyboard appearing
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWasShown(notification:)),
      name: NSNotification.Name.UIKeyboardWillShow,
      object: nil
    )
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillBeHidden(notification:)),
      name: NSNotification.Name.UIKeyboardWillHide,
      object: nil
    )
  }
  
  func deregisterFromKeyboardNotifications(){
    //Removing notifies on keyboard appearing
    NotificationCenter.default.removeObserver(
      self,
      name: NSNotification.Name.UIKeyboardWillShow,
      object: nil
    )
    NotificationCenter.default.removeObserver(
      self,
      name: NSNotification.Name.UIKeyboardWillHide,
      object: nil
    )
  }
  
  func keyboardWasShown(notification: NSNotification){
    self.scrollView.isScrollEnabled = true
    var info = notification.userInfo!
    let keyboardSize =
      (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets =
      UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
    
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    
    var aRect : CGRect = self.view.frame
    aRect.size.height -= keyboardSize!.height
    if let activeField = self.currentTextField {
      if (!aRect.contains(activeField.frame.origin)){
        self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
      }
    }
  }
  
  func keyboardWillBeHidden(notification: NSNotification){
    //Once keyboard disappears, restore original positions
    var info = notification.userInfo!
    let keyboardSize =
      (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets =
      UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    self.view.endEditing(true)
    self.scrollView.isScrollEnabled = false
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField){
    currentTextField = textField
  }
  
  func textFieldDidEndEditing(_ textField: UITextField){
    currentTextField = nil
  }
}
